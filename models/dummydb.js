const mongoose = require('mongoose')

const dummydbSchema = new mongoose.Schema({
  url: {type: String, unique: true},
  data: String
})

module.exports = mongoose.model('DummyDB', dummydbSchema)