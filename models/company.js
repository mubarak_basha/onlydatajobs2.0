const mongoose = require('mongoose')

const companySchema = new mongoose.Schema({
  name: String,
  url: String,
  twitter: String,
  totalJobs: Number
})

module.exports = mongoose.model('Company', companySchema)