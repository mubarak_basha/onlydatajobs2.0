const mongoose = require('mongoose')
mongoose.Promise = global.Promise
const slug = require('slugs')

const jobSechema = new mongoose.Schema({
  site: String,
  jobId: String,
  title: String,
  company: {type: mongoose.SchemaTypes.ObjectId, ref: 'Company'},
  location: String,
  imageUrl: String,
  description: String,
  salary: String,
  experience: String,
  remote: Boolean,
  link: {type: String, unique: true},
  publishedDate: Date,
  tags:[String],
  createdOn: {type: Date, default: Date.now},
  slug: {type: String, unique: true},
  techniques: [String],
  languages: [String],
  frameworks: [String],
  platforms: [String],
  tools: [String],
  databases: [String]
})

jobSechema.pre('save', async function(next){
  if(!this.isModified('title')){
    next()
    return
  }

  this.slug = slug(this.title)
  const slugRegEx = new RegExp(`^(${this.slug})((-[0-9]*$)?)`, 'i')
  const jobsWithSlug = await this.constructor.find({slug: slugRegEx})
  if(jobsWithSlug.length){
    this.slug = `${this.slug}-${jobsWithSlug.length + 1}`
  }
  next()
})

module.exports = mongoose.model('Job', jobSechema)