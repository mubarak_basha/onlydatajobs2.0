const siteURL = 'https://workatremote.com'
require('dotenv').config()
const mongoose = require('mongoose')
mongoose.Promise = global.Promise
mongoose.connect(process.env.CONNECTION_STRING, {useNewUrlParser: true})
  .then(mongo => console.log('1. mongoose connected to mongodb'))
  .catch(err => console.log(`1e. mongoose could not connect with mongodb ${err}`))
//models
require('./models/job')
require('./models/newsletter')
const express = require('express')
const bodyParser = require('body-parser')
const moment = require('moment')
moment.updateLocale('en', {
  relativeTime: {
    future: "in %s",
    past: "%s",
    s: '1sec',
    ss: '%dsec',
    m: "1min",
    mm: "%dmin",
    h: "1h",
    hh: "%dh",
    d: "1d",
    dd: "%dd",
    M: "1m",
    MM: "%dm",
    y: "1y",
    yy: "%dy"
  }
})
const striptags = require('striptags')
const markdown = require('markdown').markdown
const fs = require('fs')
const schedule = require('node-schedule')
const telegram = require('./telegram')(siteURL)

const crawler = require('./crawler')(siteURL)
const app = express()
app.set('view engine', 'pug')
app.use(express.static('public'))
app.use(bodyParser.urlencoded({
  extended: true
}))
app.use(bodyParser.json())


//initiate models
const Job = mongoose.model('Job')
const Newsletter = mongoose.model('Newsletter')

app.locals.moment = moment
app.locals.markdown = markdown
app.post(`/bot${telegram.telegramToken}`, (req, res) => {
  bot.processUpdate(req.body)
  res.sendStatus(200)
})

app.get('/search', (req, res) => {
  const phrase = req.query.phrase
  let tags = []
  Job.find({
      tags: new RegExp(`.*${phrase}.*`, 'i')
    }, {
      "tags.$": 1
    })
    .then(jobTags => {
      jobTags.forEach(jobTag => {
        jobTag.tags.forEach(tag => {
          if (tags.indexOf(tag) == -1) {
            tags.push(tag)
          }
        })
      })
      res.json(tags.map(tag => ({
        tag
      })))
    })
})

app.post('/newsletter', (req, res) => {
  const email = req.body.email
  const tags = req.body.tags
  let newsletter = new Newsletter({
    email,
    tags
  })
  newsletter.save()
    .then(response => {
      res.send('successfully saved!')
    })
    .catch(err => {
      res.send('some error')
    })
})

app.get('/viewJobs', (req, res) => {
  Job.find({}, ["title", "site", "category"])
    .sort({publishedDate: -1})
    .then(jobs => {
      res.render('viewJobs', {
        jobs: jobs
      })
    })
})

app.post('/editJob/:id', (req, res) => {
  Job.findOne({
      _id: req.params.id
    })
    .then(job => {
      let languages = req.body.languages
      if (languages != '') {
        languages = languages.split(',')
      } else {
        languages = []
      }
      let frameworks = req.body.frameworks
      if (frameworks != '') {
        frameworks = frameworks.split(',')
      } else {
        frameworks = []
      }
      let platforms = req.body.platforms
      if (platforms != '') {
        platforms = platforms.split(',')
      } else {
        platforms = []
      }
      let tools = req.body.tools
      if (tools != '') {
        tools = tools.split(',')
      }
      let databases = req.body.databases
      if (databases != '') {
        databases = databases.split(',')
      } else {
        databases = []
      }
      const tags = req.body.tags.split(',')
      
      job.title = req.body.title
      job.company = req.body.company
      job.twitter = req.body.twitter
      job.salary = req.body.salary
      job.timezone = req.body.timezone
      job.experience = req.body.experience
      job.degree = req.body.degree
      job.country = req.body.country
      job.languages = languages
      job.frameworks = frameworks
      job.platforms = platforms
      job.tools = tools
      job.databases = databases
      job.tags = tags
      job.save()
        .then(dbJob => {res.redirect('/viewJobs')})
        .catch(err => {console.log('unknow error ' + err)})
    })
    .catch(err => res.json({
      success: false,
      message: err
    }))
})

app.get('/editJob/:id', (req, res) => {
  Job.findOne({_id: req.params.id})
    .then(job => {
      console.log(job)
      res.render('editJob', {job})
    })
})

const getQuery = (url) => {
  let tags = []
  let findObj = null
  if (url == '/remote-programming-jobs') {
    findObj = {tags: {$in: ['programming']}}
  } else if (url == '/remote-design-jobs') {
    findObj = {tags: {$in: ['design']}}
  } else if (url == '/remote-front-end-jobs') {
    findObj = {tags: {$in: ['frontend']}}
  } else if (url == '/remote-customer-support-jobs') {
    findObj = {tags: {$in: ['customer-support']}}
  } else if (url == "/remote-marketing-jobs") {
    findObj = {tags: {$in: ['marketing']}}
  } else if (url == "/remote-other-jobs") {
    findObj = {tags: {$in: ['non-tech', 'other']}}
  } else if (url.startsWith('/remote')) {
    let tagString = url.replace('/remote-', '').replace('-jobs', '')
    tags = tagString.split('+')
    findObj = {tags: {$in: tags.map(tag => new RegExp(tag, "i"))}}
  } else if(url == '/') {
    findObj = {}
  }
  return {findObj, tags}
}

app.get('/getJobs', (req, res) => {
  const skip = parseInt(req.query.count) || 0
  const limit = 20
  const url = req.query.url
  let {findObj, tags} = getQuery(url)
  if(findObj != null){
    findObj['$or'] = [{newJob: {$exists: false}}, {newJob: false}]
    Job.find(findObj).skip(skip).limit(limit).sort({
      publishedDate: -1
    })
    .then(jobs => {
      res.render('ajaxJob', {jobs, url, tags})
    })
  } else {
    res.render({success: false, message: 'error'})
  }
  console.log('reached end........')
  return
})

app.get('/:slug?', (req, res) => {
  let url = req.url
  const obj = getQuery(url)
  let findObj = obj.findObj
  let tags = obj.tags
  if (findObj != null) {
    findObj['$or'] = [{newJob: {$exists: false}}, {newJob: false}]
    Job.find(findObj).limit(20).sort({
        publishedDate: -1
      })
      .then(jobs => {
        res.render('index', {
          jobs,
          url,
          tags,
          striptags
        })
      })
  } else {
    Job.findOne({
        slug: url.substr(1, url.length)
      })
      .then(job => {
        if(job){
          res.render('job', {
            job,
            striptags,
            tags
          })
        }
        else {
          send404(req, res)
        }
      })
  }

})

function send404(req, res){
  res.status(404)
  //respond with html page
  if(req.accepts('html')) {
    res.render('404', {url: req.url})
    return
  }
  //respond with json
  if(req.accepts('json')){
    res.send({error: 'Not found'})
    return
  }
  // default to plain-text. send()
  res.type('txt').send('Not found')
}

app.get('*', (req, res) => {
  send404(req, res)
})

const PORT = 3000
app.listen(PORT, () => {
  console.log(`2. workatremote server started at ${PORT}`)
})
//*/30 * * * *



//crawler.fetchJobs()
schedule.scheduleJob('*/30 * * * *', crawler.fetchJobs)
