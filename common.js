const fs = require('fs')
const moment = require('moment')
const parseString = require('xml2js').parseString

const exportObject = {
  isDateWithinDays : (days, date) => {
    const subractedDays = moment().subtract(days, "days").toDate()
    subractedDays.setHours(0, 0, 0, 0)
    return moment(date).isSameOrAfter(subractedDays)
  },
  promiseParseString : (data) => (new Promise((resolve, reject) => {
    parseString(data, (err, result) => {
      if(err){
        reject(err)
      }
      resolve(result)
    })
  })),

  getMatchedTags: (content, filepath) => {
    return new Promise((resolve, reject) => {
      let tags = []
      fs.readFile(filepath, (err, data) => {
        if (err) {
          reject(err)
        }
        let json = JSON.parse(data)
        for (regExMatch in json) {
          if (RegExp(regExMatch, 'i').test(content)) {
            tags.push(json[regExMatch])
          }
        }
        resolve(tags)
      })
    })
  },

  generateTagsFromContent: async content => {
    let techniques = await exportObject.getMatchedTags(content, './tags/techniques.json')
    let frameworks = await exportObject.getMatchedTags(content, './tags/frameworks.json')
    let languages = await exportObject.getMatchedTags(content, './tags/languages.json')
    let databases = await exportObject.getMatchedTags(content, './tags/databases.json')
    let platforms = await exportObject.getMatchedTags(content, './tags/platforms.json')
    let tools = await exportObject.getMatchedTags(content, './tags/tools.json')
    let tags = [...techniques, ...frameworks, ...languages, ...databases, ...platforms, ...tools]
    let twitterTags = tags.map(tag => '#'+tag.split('-').join(''))
    return {techniques, frameworks, languages, databases, platforms, tools, tags, twitterTags}
  },

  addTagsToJob: async (job) => {
    let content = job.title + ' ' + job.description
    let {techniques, frameworks, languages, databases, platforms, tools, tags, twitterTags} = await exportObject.generateTagsFromContent(content)
    job['techniques'] = techniques
    job['frameworks'] = frameworks
    job['languages'] = languages
    job['databases'] = databases
    job['platforms'] = platforms
    job['tools'] = tools
    job['tags'] = tags
    //TODO - In the future add automatic timezone detection.
    job['timezone'] = ''
    return {job, twitterTags}
  }

  
}

module.exports = exportObject