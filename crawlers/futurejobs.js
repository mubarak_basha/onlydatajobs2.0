const mongoose = require('mongoose')
const axios = require('axios')
const parseString = require('xml2js').parseString
const html2text = require('html-to-text')
const common = require('../common')
const Job = mongoose.model('Job')

const links = {
  'data-science': 'https://www.futurejobs.io/categories/data-science.rss',
  'machine-learning': 'https://www.futurejobs.io/categories/machine-learning.rss',
  'blockchain': 'https://www.futurejobs.io/categories/blockchain.rss',
  'research-science': 'https://www.futurejobs.io/categories/research-science.rss'
}

const parseStringPromise = xmlData => new Promise((resolve, reject) => {
  parseString(xmlData, (err, result) => {
    if(err) {
      reject(err)
    }
    resolve(result)
  })
})

module.exports = (category, durationInDays) => {
  return axios.get(links[category])
    .then(response => {
      return Promise.all([parseStringPromise(response.data), Job.find({site: 'futurejobs.io'}, ['link'])])
    })
    .then(([result, dbJobLinks]) => {
      dbJobLinks = dbJobLinks.map(dbJob => dbJob.link)
      const jobs = []
      result.rss.channel[0].item.forEach(item => {
        const publishedDateString = item.pubDate[0]
        const link = item.link[0]
        if(common.isDateWithinDays(durationInDays, new Date(publishedDateString)) && dbJobLinks.indexOf(link) == -1){
          const imageUrl = item['media:content'] != undefined ? item['media:content'].url : null
          let title = item.title[0]
          const company = title.substr(0, title.indexOf(':')).trim()
          title = title.substr(title.indexOf(':')+1, title.length).trim()
          const description = html2text.fromString(item.description[0].replace(/<img[^>]*>/g, ""), {wordwrap: false})
          let location = description.substring(item.description[0].indexOf("/strong>") + 1, description.indexOf('<br />')).replace("strong>", "").replace("\n", "").trim()
          const id = item.guid[0]
          jobs.push({
            site: 'futurejobs.io',
            jobId: id,
            title: title,
            company: company,
            location: location,
            imageUrl: imageUrl,
            description: description,
            salary: '',
            experience: '',
            remote: false,
            link: link,
            publishedDate: new Date(publishedDateString)
          })
        }
      })
      console.log('futurejobs count ', jobs.length)
      return Promise.resolve(jobs)
    })
    .catch(error => {
      console.error(`futurejobs: ${category} error while retriving \n ${error}`)
    })
}