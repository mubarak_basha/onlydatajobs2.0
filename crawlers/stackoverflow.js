const axios = require('axios')
const cheerio = require('cheerio')
const mongoose = require('mongoose')
const Job = mongoose.model('Job')
const Company = mongoose.model('Company')
const DummyDB = mongoose.model('DummyDB')
const common = require('../common')
const fs = require('fs')
const log = (msg) => console.log(`stackoverflow: ${msg}`)

//1. parse job feed
const searchPageResponse = () => (
  axios.get('https://stackoverflow.com/jobs/feed?tl=machine-learning+computer-vision+artificial-intelligence+nlp+data-analysis+pandas+theano+tensorflow+keras+pytorch+lstm+scipy+matplotlib+scikit-learn+cntk+nltk+apache-spark+conv-neural-network+recurrent-neural-network+neural-network+bigdata+apache-kafka+hadoop+hive+apache-pig+presto+apache-spark-sql+apache-airflow+scala+sqoop+hbase+flume+h2o+r+matlab')
    .then(response => Promise.resolve(response.data))

)

//2. filter jobs which are already exist in db
const parseJobs = (durationInDays, xmlData) => {
  return Promise.all([
    common.promiseParseString(String(xmlData)),
    Job.find({site: 'stackoverflow.com'}, ['link'])
  ])
  .then(([xmlJobList, dbJobLinks]) => {
    dbJobLinks = dbJobLinks.map(dbJob => dbJob.link)
    let overviewJobs = []
    const rssJobs = xmlJobList.rss.channel[0].item
    for(let i=0; i < rssJobs.length; i++){
      let item = rssJobs[i]
      let publishedDate = new Date(item.pubDate[0])

      if(common.isDateWithinDays(durationInDays, publishedDate)){
        let link = item.link[0]
        if(dbJobLinks.indexOf(link) == -1){
          overviewJobs.push({
            site: 'stackoverflow.com',
            jobId: item.guid[0]._,
            title: item.title[0],
            company: item['a10:author'][0]['a10:name'][0],
            description: item.description[0],
            tags: item.category,
            publishedDate: new Date(item.pubDate[0]),
            link
          })
        }
      }
    }
    return Promise.resolve(overviewJobs)
  })
  .catch(err => {
    console.log('err', err)
  })
}

//3. fetch extra details from the job page
const fetchJobDetails = (job) => {
  return axios.get(job.link)
    .then(response => {
      jobPage$ = cheerio.load(response.data)
      const imageUrl = jobPage$('.job-details--header .hmx100').attr('src')
      const location = jobPage$('.job-details--header .fs-body3 .fc-black-500').text().replace('|', '').replace('\n', '').trim()
      const salary = jobPage$('.job-details--header span.-salary').text().replace(/\|?\s*equity/gi, '').trim()
      const remote = jobPage$('.job-details--header span.-remote').text() ? true : false
      const companyUrl = jobPage$('.job-details--header a.fc-black-700').attr('href')
      return Promise.resolve(Object.assign({imageUrl, location, salary, remote, companyUrl}, job))
    })
    .catch(error => {
      console.log(error)
    })
}

//4. return the job array

const delay = (ms) => {
  return new Promise(resolve => {
    console.log('added new job')
    setTimeout(resolve, ms)
  })
}


module.exports = {
  getJobs: (durationInDays) => {
    return searchPageResponse()
      .then(xmlData => parseJobs(durationInDays, xmlData))
      .then(overviewJobs => {
        let jobPromises = []
        overviewJobs.forEach((overviewJob, index) => {
          jobPromises.push(delay(index * 5000).then(() => fetchJobDetails(overviewJob)))
        })
        return Promise.all(jobPromises)
      })
  },
  getCompanyTwitterAndUrl: (companyUrl) => {
    if(companyUrl != undefined && companyUrl.startsWith('/')){
      return axios.get(`https://stackoverflow.com${companyUrl}`)
      .then(response => {
        const company$ = cheerio.load(response.data)
        let hrefs = company$('#right-column span.ps-absolute a').map(function(index, element) { return company$(this).attr('href') }).get()
        let twitterUsername = hrefs.reduce((acc, curr) => {
          if(curr.includes('twitter')){
            acc = curr
          }
          return acc
        }, '')
        if(twitterUsername != ''){
          twitterUsername = '@'+twitterUsername.replace(/.*%2f(.*)&externalLink=Twitter/ig, '$1')
        }

        let website = decodeURIComponent(company$('#right-column span.d-block a').attr('href'))
        if(website != null){
          website = website.replace(/.*Url=(.*)&externalLink=Website/ig, '$1')
        }
        return Promise.resolve({twitterUsername, website})
      })
      .catch(error => {
        console.log(`error while fetching company's twitter and website: ${error}`)
      })
    } else {
      return Promise.resolve(null)
    }
  }
}