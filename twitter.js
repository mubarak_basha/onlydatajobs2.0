const twitter = require('twitter')

const client = new twitter({
  consumer_key: process.env.TWITTER_CONSUMER_KEY,
  consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
  access_token_key: process.env.TWITTER_ACCESS_TOKEN_KEY,
  access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET
})
const postTweet = (content) => {
  client.post('statuses/update', {status: content}, function(error, tweet, response) {
    if(error){
      console.log('twitter status posting error: ', error)
    }
  })
}

module.exports = {postTweet}