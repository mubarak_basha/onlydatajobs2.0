const request = require('request')
const {google} = require('googleapis')
const key = require('./google_keys.json')
const jwtClient = new google.auth.JWT(
  key.client_email,
  null,
  key.private_key,
  ["https://www.googleapis.com/auth/indexing"],
  null
);
const submitURLForIndexing = (url) => {
  return new Promise((resolve, reject) => {
    jwtClient.authorize((err, tokens) => {
      if(err){
        reject(err)
      }
      let options = {
        url: "https://indexing.googleapis.com/v3/urlNotifications:publish",
        method: "POST",
        // Your options, which must include the Content-Type and auth headers
        headers: {
          "Content-Type": "application/json"
        },
        auth: { "bearer": tokens.access_token },
        // Define contents here. The structure of the content is described in the next step.
        json: {
          "url":  url,
          "type": "URL_UPDATED"
        }
      }
      request(options, function (error, response, body) {
        if(error){
          reject(error)
        }
        resolve(body)
      });
    })
  })
}

module.exports = {submitURLForIndexing}