//node modules
const fs = require('fs')
//installed modules
const moment = require('moment')
const mongoose = require('mongoose')
const config = require('./config')
const common = require('./common')
//custom modules
const google = require('./google')
const twitter = require('./twitter')
const telegram = require('./telegram')('https://onlydatajobs.com')
let stackoverflow = require('./crawlers/stackoverflow')
let futurejobs = require('./crawlers/futurejobs')
//models
const Job = mongoose.model('Job')
const Company = mongoose.model('Company')

module.exports = (siteURL) => {
  const saveJob = (company, job, twitterTags) => {
    let newJob = new Job({
      site: job.site,
      jobId: job.jobId,
      title: job.title,
      company: company._id,
      location: job.location,
      imageUrl: job.imageUrl,
      description: job.description,
      salary: job.salary,
      experience: '',
      remote: job.remote,
      link: job.link,
      publishedDate: job.publishedDate,
      tags: job.tags,
      techniques: job.techniques,
      languages: job.languages,
      frameworks: job.frameworks,
      platforms: job.platforms,
      tools: job.tools,
      databases: job.databases
    })
    newJob.save()
    .then(response => {
      console.log('company', company)
      console.log(`Job saved ${job.site}: ${job.title}`)
      //notify google about new job.
      google.submitURLForIndexing(`${siteURL}/${response.slug}`)
         .then(`url: ${siteURL}/${response.slug} submitted to google.`)
      //send message to mubarak that a new job has been posted.
      telegram.sendMessageToMubarak(`Job Published title: ${response.title} <a href="/editJob?id=${response._id}">Edit</a>`)
      //tweet the latest job
      twitter.postTweet(`${company.twitter ? company.twitter : company.name} is looking for ${response.title} learn more at ${siteURL}/${response.slug} #remotework #remotejob #job ${twitterTags.join(',')}`)
      //post a message in work@remote telgram group
      telegram.sendMessageToGroup(`<a href="${siteURL}/${response.slug}">${response.title}</a>\nCompany: ${company.name}\n${response.salary != ''? 'Salary: '+response.salary : ''}\n${response.experience != ''? 'Exp: '+response.experience : ''}\n${response.country != ''? '<b>'+response.country+'</b>' : ''}\nTags: ${twitterTags.join(',')}`)
    })
    .catch(err => {
      console.log(`error saving job ${job} Error: ${err}`)
    })
  }

  const fetchJobs = async () => {
    let allJobs = {}
    stackoverflow.getJobs(config.fetchJobWithinDays)
        .then(jobs => {

          jobs.forEach(async stackJob => {
            let company = await Company.findOne({name: stackJob.company})
            if(!company){
              const companyDetails = await stackoverflow.getCompanyTwitterAndUrl(stackJob.companyUrl)
              if(companyDetails != null) {
                company = new Company({name: stackJob.company, url: companyDetails.website, twitter: companyDetails.twitterUsername})

              }
              else {
                company = new Company({name: stackJob.company})
              }
              company = await company.save()
            }
            let {job, twitterTags} = await common.addTagsToJob(stackJob)
            saveJob(company, job, twitterTags)
          })
          return futurejobs('data-science', config.fetchJobWithinDays)
        })
        .then(jobs => {
          allJobs['data-science'] = jobs
          return futurejobs('machine-learning', config.fetchJobWithinDays)
        })
        .then(jobs => {
          allJobs['machine-learning'] = jobs
          return futurejobs('blockchain', config.fetchJobWithinDays)
        })
        .then(jobs => {
          allJobs['blockchain'] = jobs
          return futurejobs('research-science', config.fetchJobWithinDays)
        })
        .then(jobs => {
          allJobs['research-science'] = jobs
          return Promise.resolve(allJobs)
        })
        .then(jobs => {
          let concatJobs = []
          for(let key in jobs){
            if(jobs.hasOwnProperty(key)){
              concatJobs = concatJobs.concat(jobs[key])
            }
          }
          concatJobs.forEach(async futureJob => {
            let company = await Company.findOne({name: futureJob.company})
            if(!company){
              company = new Company({name: futureJob.company})
              company = await company.save()
            }
            let {job, twitterTags} = await common.addTagsToJob(futureJob)
            saveJob(company, job, twitterTags)
          })
          
        })
        .catch(error => {
          console.log('error', error)
        })
  }
  return {
    fetchJobs
  }
}